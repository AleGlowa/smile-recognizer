import os                               # for path operations
import numpy as np                      # for converting list to numpy array
import scipy.io as sio                  # for loading mat files (they contain face landmarks for each frame)
import cv2                              # for getting frames from videos and preprocess them
import re                               # for encode classes
from useful_utils import FaceAligner    # for preprocess images
from useful_utils import indicies_videos, no_frames_feach_video

arr_landmarks = []

reg_lowintensity = re.compile(r'^\d+,[1-3]')
reg_highintensity = re.compile(r'\d+,[4-5]')
reg_negative = re.compile(r'^\d+,0')

videos_path = 'data/videos/'
landmarks_path = 'data/landmarks/'
labels_path = 'data/labels/'

frames_path = 'preprocessed_data/frames/'

for idx_video in indicies_videos:
    vidcap = cv2.VideoCapture(os.path.join(videos_path, 'SN%03d/LeftVideoSN%03d_comp.avi' % (idx_video, idx_video)))
    # add landmarks from a video
    for i in range(used_frames):
        if (idx_video >= 1 and idx_video < 8) or (idx_video >= 11 and idx_video < 21):
            arr_landmarks.append(sio.loadmat(os.path.join(landmarks_path, 'SN%03d/SN%03d_%04d_lm.mat' % (idx_video, idx_video, i)))['pts'])
        else:
            arr_landmarks.append(sio.loadmat(os.path.join(landmarks_path, 'SN%03d/l%05d_lm.mat' % (idx_video, (i + 1))))['pts'])
    index = 0

    while index < used_frames:
        success, image = vidcap.read()
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        landmark = arr_landmarks[index]

        # align faces
        faceAligner = FaceAligner()
        arr_landmarks[index] = np.array(arr_landmarks[index])
        aligned = faceAligner.align(gray_image, landmarks=arr_landmarks[index])

        if not success:
            break

        # save aligned faces
        cv2.imwrite(os.path.join(frames_path + 'SN%03d/' % idx_video, 'frame%d.jpg' % index), aligned)
        print('Saved frame ', index + 1, ' from video index ', video_idx)
        index += 1

    # preprocess label files
    file_path = 'data/labels/SN%03d/SN%03d_au12.txt' % (idx_video, idx_video)
    with open(file_path, 'r') as old_file:
        lines = old_file.readlines()
        
    new_file = 'data/labels/SN%03d/SN%03d_au12_preprocessed.txt' % (idx_video, idx_video)
    with open(new_file, 'w') as file:
        for line in lines:
            if reg_highintensity.match(line):
                file.write(reg_highintensity.sub('2', line))
            elif reg_lowintensity.match(line):
                file.write(reg_lowintensity.sub('1', line))
            elif reg_negative.match(line):
                file.write(reg_negative.sub('0', line))

    arr_landmarks = []
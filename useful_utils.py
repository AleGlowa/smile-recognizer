import numpy as np                      # for shape_to_np
import cv2                              # for output for align method
from collections import OrderedDict     # for facial landmarks

target_size = (285, 378)
no_class = 3
no_frames_feach_video = 4844
indicies_videos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 21, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]   # 27 videos
dir_X = 'preprocessed_data/frames/'
dir_Y = 'data/labels/'

FACIAL_LANDMARKS_68_IDXS = OrderedDict([
    ('mouth', (48, 68)),
    ('inner_mounth', (60, 68)),
    ('right_eyebrow', (17, 22)),
    ('left_eyebrow', (22, 27)),
    ('right_eye', (36, 42)),
    ('left_eye', (42, 48)),
    ('nose', (27, 36)),
    ('jaw', (0, 17))
])

FACIAL_LANDMARKS_IDXS = FACIAL_LANDMARKS_68_IDXS

def shape_to_np(shape, dtype='int'):
    coords = np.zeros((shape.num_parts, 2), dtype=dtype)
    for i in range(0, shape.num_parts):
        coords[i] = (shape.part(i).x, shape.part(i).y)

    return coords

def rect_to_bb(rect):
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    return (x, y, w, h)

class FaceAligner:
    def __init__(self, predictor=None, desiredLeftEye=(0.35, 0.35), desiredFaceWidth=target_size[0], desiredFaceHeight=target_size[1]):
        self.predictor = predictor
        self.desiredLeftEye = desiredLeftEye
        self.desiredFaceWidth = desiredFaceWidth
        self.desiredFaceHeight = desiredFaceHeight

        # if the desired face height is None, set it to be the
        # desired face width (normal behavior)
        if self.desiredFaceHeight is None:
            self.desiredFaceHeight = self.desiredFaceWidth
          
    
    def align(self, gray_image, landmarks=None, rect=None):
        if landmarks is None and rect is not None:
            shape = self.predictor(gray_image, rect)
            shape = shape_to_np(shape)

            (lStart, lEnd) = FACIAL_LANDMARKS_IDXS['left_eye']
            (rStart, rEnd) = FACIAL_LANDMARKS_IDXS['right_eye']
            leftEyePts = shape[lStart:lEnd]
            rightEyePts = shape[rStart:rEnd]
        
        else:
            # convert the landmark (x, y)-coordinates to a NumPy array
            # from 37 to 49 landmarks IDs notice eyes positions (6 landmarks for each eye)
            eyes_landmarks = landmarks[36:48]
            leftEyePts = eyes_landmarks[6:12]
            rightEyePts = eyes_landmarks[:6]
        
        # compute the center of mass for each eye
        leftEyeCenter = leftEyePts.mean(axis=0).astype('int')
        rightEyeCenter = rightEyePts.mean(axis=0).astype('int')

        # compute the angle between the eye centroids
        dY = rightEyeCenter[1] - leftEyeCenter[1]
        dX = rightEyeCenter[0] - leftEyeCenter[0]
        angle = np.degrees(np.arctan2(dY, dX)) - 180

        desiredRightEyeX = 1.0 - self.desiredLeftEye[0]
        dist = np.sqrt((dX**2) + (dY**2))
        desiredDist = (desiredRightEyeX - self.desiredLeftEye[0])
        desiredDist *= self.desiredFaceWidth
        scale = desiredDist / dist

        eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2, (leftEyeCenter[1] + rightEyeCenter[1]) // 2)
        M = cv2.getRotationMatrix2D(eyesCenter, angle, scale)

        tX = self.desiredFaceWidth * 0.5
        tY = self.desiredFaceHeight * self.desiredLeftEye[1]
        M[0, 2] += (tX - eyesCenter[0])
        M[1, 2] += (tY - eyesCenter[1])

        (w, h) = (self.desiredFaceWidth, self.desiredFaceHeight)
        output = cv2.warpAffine(gray_image, M, (w, h), flags=cv2.INTER_CUBIC)


        return output

def get_data_set():
    images = []
    for video in indicies_videos:
        for frame in range(no_frames_feach_video):
            images.append(dir_X + 'SN%03d/frame%d.jpg' % (video, frame))

    classes = []
    for video in indicies_videos:
        with open(dir_Y + 'SN%03d/SN%03d_au12_preprocessed.txt' % (video, video), 'r') as file:
            lines = file.readlines()[:no_frames_feach_video]
            for line in lines:
                classes.append(int(line))

    return images, classes
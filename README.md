# **Smile Recognizer**

Project component for "artificial intelligence" subject.
It recognizes smiles on levels ('no smile', 'low intensity smile', 'high intensity smile') by using images of faces.